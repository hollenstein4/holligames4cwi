package at.hollen.games.wintergame;


import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflake implements Actor {
	private float width;
	private Size size;
	public enum Size {
		small, medium, big
	}
	private float speed;
	//private List<Snowflake> snowflakes;
	private Random r = new Random();
	private int x = r.nextInt(500);
	private float y = r.nextInt(500)*-1;
	
	
	
	public Snowflake(Snowflake.Size size) {
		super();
		this.size = size;
		
		if (this.size == Size.small) {
			this.speed = 0.2f;
			this.width = 5;
		} else if (this.size == Size.medium) {
			this.speed = 0.3f;
			this.width = 10;
		}else if (this.size == Size.big) {
			this.speed = 0.5f;
			this.width = 15;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, this.width, this.width);
	}
	
	public void update(GameContainer gc, int delta) {
		this.y = this.y + delta * this.speed;
		//hier noch speed einf�gen
		
		
		
		if (this.y>500) {
			this.y = -15;
			this.x = r.nextInt(500);
		}
	}

	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
