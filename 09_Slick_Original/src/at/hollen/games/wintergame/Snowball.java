package at.hollen.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowball implements Actor{
	private float x;
	private float y;
	private int size;
	private boolean isOutOfGame;

	public Snowball(int y) {
		super();
		this.x = 60;
		this.y = y+30;
		this.size = 10;
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (this.x <= 500) {
			this.x = this.x + delta * 0.5f;
		} else {
			setOutOfGame(true);
		}
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, (int) (this.size), (int) (this.size));
		
	}

	public boolean isOutOfGame() {
		return isOutOfGame;
	}

	public void setOutOfGame(boolean isOutOfGame) {
		this.isOutOfGame = isOutOfGame;
	}


	
	

}
