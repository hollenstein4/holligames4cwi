package at.hollen.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor implements Actor {
	private double x, y;

	public CircleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void render(Graphics graphics) {
		graphics.drawOval((float) this.x, (float) this.y, 50, 50);
	}

	public void update(GameContainer gc, int delta) {
		this.y = y + 0.5 * delta;
		if (this.y > 500) {
			this.y = -50;
		}
	}

	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}

}
