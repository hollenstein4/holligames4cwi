package at.hollen.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor {
	private double x, y;
	private boolean ovalRightDirection = true;

	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void render(Graphics graphics) {
		graphics.drawOval((float) this.x, (float) this.y, 100, 50);
	}

	public void update(GameContainer gc, int delta) {
		if (ovalRightDirection) {
			if (x > 400) {
				ovalRightDirection = false;
			} else {
				this.x = x + 0.5 * delta;
			}
		} else {
			if (this.x < 0) {
				ovalRightDirection = true;
			} else {
				this.x = x - 0.5 * delta;
			}
		}
	}

	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}

}
