package at.hollen.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor implements Actor {
	private double x, y;
	private dir Direction;
	private enum dir {
		right, down, left, up
	}
	

	public RectActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.Direction = dir.right;
	}

	
	public void render(Graphics graphics) {
		graphics.drawRect((float)this.x, (float)this.y, 50, 50);
	}
	public void update(GameContainer gc, int delta) {
		if (this.Direction == dir.right) {
			if (x>=450) {
				this.Direction = dir.down;
			} else {
				x = x + 0.5 * delta;
			}
		} else if (this.Direction == dir.down) {
			if (y>=450) {
				this.Direction = dir.left;
			} else {
				y = y + 0.5 * delta;
			}
		} else if (this.Direction == dir.left) {
			if (x<=0) {
				this.Direction = dir.up;
			} else {
				x = x - 0.5 * delta;
			}
		} else if (this.Direction == dir.up) {
			if (y<=0) {
				this.Direction = dir.right;
			} else {
				y = y - 0.5 * delta;
			}
		}
	}


	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}
	

}
