package at.hollen.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;



public class Shootingstar implements Actor {
	private float width;
	//private float size;
	private int direction;
	private float directionValue;
	private float widthValue;
	private Random r = new Random();
	private float x;
	private float y;
	private long counter;
	
	
	public Shootingstar() {
		super();
		//this.size = 15;
		setRandomPostionAndDirection();
	}



	private void setRandomPostionAndDirection() {
		
		this.counter = 0;
		this.x = r.nextInt(500);
		this.y = r.nextInt(500);
		this.width = 10;
		this.directionValue = 0.1f;
		this.widthValue = 0.05f;
		if(this.x < 250) {
			this.direction = 1;
		} else {
			this.direction = -1;
		}
	}

	

	@Override
	public void update(GameContainer gc, int delta) {
		if (this.width > 0) {
			this.directionValue = this.directionValue - 0.0001f * delta;
			this.y = this.y - this.directionValue * delta;
			this.x = this.x + (0.15f * this.direction * delta);			//move in x-Axis
	
			this.widthValue = this.widthValue - 0.00005f * delta;
			this.width = this.width + this.widthValue * delta;
		} else {
			counter += delta;
			   if (counter >= y*4) {		//wait-time for next shootingstar based on the random number of "y"
			      counter = 0;
					setRandomPostionAndDirection();
			   }
		}
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.fillOval((int) this.x, (int) this.y, (int) this.width, (int) this.width);
		graphics.setColor(Color.white);
		
	}



	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}

}
