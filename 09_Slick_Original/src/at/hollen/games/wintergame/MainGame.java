package at.hollen.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private List<Actor> actors;
	private List<Actor> cleanUpActors;
	private int shootInterval;
	private Snowman snowman;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {

		this.actors = new ArrayList<>();
		this.cleanUpActors = new ArrayList<>();
		this.shootInterval = 100;

		this.actors.add(new RectActor(0, 0));
		this.actors.add(new OvalActor(0, 225));
		this.actors.add(new CircleActor(225, 0));

		// this.snowflakes = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.small));
			this.actors.add(new Snowflake(Snowflake.Size.medium));
			this.actors.add(new Snowflake(Snowflake.Size.big));
		}

		Shootingstar shoot1 = new Shootingstar();
		this.actors.add(shoot1);
		this.snowman = new Snowman();
		this.actors.add(snowman);
		// this.actors.remove(shoot1);

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {

		for (Actor actor : this.actors) {
			actor.update(gc, delta);
			
			if (actor.isOutOfGame()) {
				this.cleanUpActors.add(actor);
			}
		}
		
		for (Actor actor : this.cleanUpActors) {
			this.actors.remove(actor);
			
		}
		this.cleanUpActors.clear();

		this.shootInterval = this.shootInterval + delta;
		if(shootInterval > 100) {
			if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {
				this.actors.add(new Snowball(this.snowman.getY()));
				
		    }
			this.shootInterval = 0;
		}
		
	}
/*
	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		System.out.println("pressed");
	}
*/
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(500, 500, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
