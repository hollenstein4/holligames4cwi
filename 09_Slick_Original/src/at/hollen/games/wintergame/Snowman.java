package at.hollen.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Snowman implements Actor {
	private int waitTime;
	private int size;
	private int x;
	private int y;

	private List<Actor> snowballs;
	private List<Actor> cleanUpSnowballs;
	
	
	
	public Snowman() {
		super();
		this.waitTime = 0;
		this.size = 30;
		this.x = 50;
		this.y = 400;
		this.snowballs = new ArrayList<>();
		this.cleanUpSnowballs = new ArrayList<>();
	}
	

	@Override
	public void update(GameContainer gc, int delta) {
		
		this.waitTime = this.waitTime + delta;
		if(waitTime > 10) {
			if (gc.getInput().isKeyDown(Input.KEY_UP)) {
				if (this.y >= 0) {
					this.y = (int) (this.y - waitTime * 0.1f);
				}
		    }
			if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
				if (this.y <= 390) {
		        	this.y = (int) (this.y + waitTime * 0.2f);
				}
		    }
			this.waitTime = 0;
		}
		
		
		
		for (Actor actor : this.snowballs) {
			actor.update(gc, delta);
			actor.render(gc.getGraphics());
			
			if (actor.isOutOfGame()) {
				this.cleanUpSnowballs.add(actor);
			}
		}
		/*
		for (Actor actor : this.cleanUpSnowballs) {
			this.snowballs.remove(actor);
			
		}
		this.cleanUpSnowballs.clear();
		*/
	}
	@Override
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, (int) (this.size), (int) (this.size));
		graphics.fillOval((int) (this.x - this.size * 0.25), (int) (this.y + this.size * 0.75), (int) (this.size * 1.5), (int) (this.size * 1.5));
		graphics.fillOval((int) (this.x - this.size * 0.5), (int) (this.y + this.size * 1.75), (int) (this.size * 2), (int) (this.size * 2));
	}
	@Override
	public boolean isOutOfGame() {
		// TODO Auto-generated method stub
		return false;
	}


	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}
	
	

}
